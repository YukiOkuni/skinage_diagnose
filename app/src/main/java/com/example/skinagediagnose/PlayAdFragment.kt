package com.example.skinagediagnose

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_playad.*
import net.nend.android.NendAdRewardItem
import net.nend.android.NendAdRewardedListener
import net.nend.android.NendAdRewardedVideo
import net.nend.android.NendAdVideo

class PlayAdFragment : Fragment() {

    private lateinit var nendInterstitialVideo: NendAdRewardedVideo

    //実装用
//    val NEND_INTERSTITIAL_ID: Int =
//    val NEND_INTERSTITIAL_KEY: String = ""

    //テスト用
    val NEND_INTERSTITIAL_ID: Int = 802558
    val NEND_INTERSTITIAL_KEY: String = "a6eb8828d64c70630fd6737bd266756c5c7d48aa"

    private var changeListener: MyListener.ChangeFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentManager
            ?.beginTransaction()
            ?.hide(fragmentManager?.findFragmentByTag(tag_result)!!)
            ?.commitAllowingStateLoss()

        loadVideoAd()

        return inflater.inflate(R.layout.fragment_playad, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        changeListener = context as MyListener.ChangeFragmentListener
    }

    override fun onDetach() {
        super.onDetach()

        changeListener = null
    }

    override fun onStart() {
        super.onStart()


    }

    fun onFragmentStart() {
        go_result.text = "OK"

        go_result.setOnClickListener(View.OnClickListener { view: View ->
            playAd()

            go_result.text = "ロード中..."
        })
    }

    fun playAd() {
        nendInterstitialVideo.setAdListener(object : NendAdRewardedListener {
            override fun onRewarded(p0: NendAdVideo?, p1: NendAdRewardItem?) {
            }

            override fun onLoaded(p0: NendAdVideo?) {
                nendInterstitialVideo.showAd(activity)
            }

            override fun onFailedToLoad(p0: NendAdVideo?, p1: Int) {
                Log.w("dev", "errorCode: $p1")

                changeListener?.onChangeFragment(tag_result)

                fragmentManager
                    ?.beginTransaction()
                    ?.hide(fragmentManager?.findFragmentByTag(tag_playAd)!!)
                    ?.show(fragmentManager?.findFragmentByTag(tag_result)!!)
                    ?.commitAllowingStateLoss()
            }

            override fun onFailedToPlay(p0: NendAdVideo?) {

                changeListener?.onChangeFragment(tag_result)

                fragmentManager
                    ?.beginTransaction()
                    ?.hide(fragmentManager?.findFragmentByTag(tag_playAd)!!)
                    ?.show(fragmentManager?.findFragmentByTag(tag_result)!!)
                    ?.commitAllowingStateLoss()
            }

            override fun onShown(p0: NendAdVideo?) {
            }

            override fun onClosed(p0: NendAdVideo?) {
            }

            override fun onStarted(p0: NendAdVideo?) {
            }

            override fun onStopped(p0: NendAdVideo?) {
            }

            override fun onCompleted(p0: NendAdVideo?) {
                changeListener?.onChangeFragment(tag_result)

                fragmentManager
                    ?.beginTransaction()
                    ?.hide(fragmentManager?.findFragmentByTag(tag_playAd)!!)
                    ?.show(fragmentManager?.findFragmentByTag(tag_result)!!)
                    ?.commitAllowingStateLoss()
            }

            override fun onAdClicked(p0: NendAdVideo?) {
            }

            override fun onInformationClicked(p0: NendAdVideo?) {
            }
        })

        if (nendInterstitialVideo.isLoaded)
            nendInterstitialVideo.showAd(activity)
    }

    fun loadVideoAd() {
        nendInterstitialVideo = NendAdRewardedVideo(context, NEND_INTERSTITIAL_ID, NEND_INTERSTITIAL_KEY)
        nendInterstitialVideo.loadAd()
    }
}