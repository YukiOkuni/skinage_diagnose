package com.example.skinagediagnose

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_diagnose.*
import net.nend.android.NendAdNativeMediaView
import net.nend.android.NendAdNativeMediaViewListener
import net.nend.android.NendAdNativeVideo
import net.nend.android.NendAdNativeVideoLoader

const val SCORE = "SCORE"

class DiagnoseFragment : Fragment() {

    private var changeListener: MyListener.ChangeFragmentListener? = null
    private var changeQuizListener: MyListener.ChangeQuizListener? = null
    private var checkFragmentListener: MyListener.CheckedCurrentFragmentListener? = null

    lateinit var nendLoader:NendAdNativeVideoLoader
    lateinit var videoAd:NendAdNativeVideo

    //実装用
//    val NEND_VIDEO_ID:Int =
//    val NEND_VIDEO_KEY: String = ""

    //テスト用
    val NEND_VIDEO_ID:Int = 887591
    val NEND_VIDEO_KEY: String = "a284d892c3617bf5705facd3bfd8e9934a8b2491"

    var currentQuizNum = 0
    var currentScore = 0
    val handler = Handler()
    private lateinit var runnable: Runnable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        fragmentManager
            ?.beginTransaction()
            ?.hide(fragmentManager?.findFragmentByTag(tag_result)!!)
            ?.commitAllowingStateLoss()

        nendLoader = NendAdNativeVideoLoader(context,NEND_VIDEO_ID,NEND_VIDEO_KEY)

        return inflater.inflate(R.layout.fragment_diagnose, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        changeListener = context as MyListener.ChangeFragmentListener
        changeQuizListener = context as MyListener.ChangeQuizListener
        checkFragmentListener = context as MyListener.CheckedCurrentFragmentListener
    }

    override fun onDetach() {
        super.onDetach()

        changeListener = null
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onPause() {
        super.onPause()

        if(::runnable.isInitialized)
            handler.removeCallbacks(runnable)
    }

    private fun setQuiz() {
        if (SkinAgeContract.quizSet.size <= 0 || SkinAgeContract.resultSet.size <= 0)
            return
        if (currentQuizNum >= SkinAgeContract.quizSet.size)
            return

        changeQuizListener?.onChangeQuiz(currentQuizNum)

        val ans = mutableListOf(
            SkinAgeContract.quizSet[currentQuizNum][1],
            SkinAgeContract.quizSet[currentQuizNum][2],
            SkinAgeContract.quizSet[currentQuizNum][3]
        )
        ans.shuffle()

        ans1_button.text = ans[0]
        ans2_button.text = ans[1]
        ans3_button.text = ans[2]
        quiz_text.text = SkinAgeContract.quizSet[currentQuizNum][0]
    }

    private fun answerClicked(clicked: Button) {
        if (currentQuizNum < SkinAgeContract.quizSet.size && currentQuizNum >= 0) {
            if (clicked.text.equals(SkinAgeContract.quizSet[currentQuizNum][1])) {
                currentScore++
            }

            currentQuizNum++

            if (currentQuizNum >= SkinAgeContract.quizSet.size) {
                goPlayAd()
            } else {
                setQuiz()
            }
        }
    }

    fun onFragmentStart() {
        loadNendVideoAd()

        currentQuizNum = 0
        currentScore = 0

        SkinAgeContract.quizAll.shuffle()
        SkinAgeContract.quizSet = mutableListOf()
        for (i in 0..SkinAgeContract.quizNum - 1)
        {
            SkinAgeContract.quizSet.add(SkinAgeContract.quizAll[i])
        }

        setQuiz()

        ans1_button.setOnClickListener(View.OnClickListener { view: View ->

            answerClicked(view as Button)
        })

        ans2_button.setOnClickListener(View.OnClickListener { view: View ->

            answerClicked(view as Button)
        })

        ans3_button.setOnClickListener(View.OnClickListener { view: View ->

            answerClicked(view as Button)
        })
    }

    fun goPlayAd() {
        changeListener?.onChangeFragment(tag_playAd)

        val bundle = Bundle()
        bundle.putInt(SCORE, currentScore)

        (fragmentManager?.findFragmentByTag(tag_result) as ResultFragment).arguments = bundle

        fragmentManager
            ?.beginTransaction()
            ?.hide(fragmentManager?.findFragmentByTag(tag_diagnose)!!)
            ?.show(fragmentManager?.findFragmentByTag(tag_playAd)!!)
            ?.commitAllowingStateLoss()
    }

    fun loadNendVideoAd()
    {
        nendLoader.loadAd(object: NendAdNativeVideoLoader.Callback{
            override fun onSuccess(ad: NendAdNativeVideo?) {
                if(ad != null && ad.hasVideo()){

                    if(checkFragmentListener != null && checkFragmentListener!!.onCheckedCurrentFragment() == tag_diagnose) {
                        videoAd = ad

                        setNendVideoAd()
                    }
                }
            }

            override fun onFailure(errorCode: Int) {
                Log.i("dev", "広告取得失敗：${errorCode}")
            }
        })
    }

    fun setNendVideoAd()
    {
        ad2_video.setMedia(videoAd)
        ad2_title.text = videoAd.titleText
        ad2_description.text = videoAd.descriptionText
        ad2_advertiser_name.text = "PR：${videoAd.advertiserName}"
        ad2_action.text = videoAd.callToActionText

        ad2_video.setMediaViewListener(object: NendAdNativeMediaViewListener{
            override fun onStopPlay(p0: NendAdNativeMediaView?) {
            }
            override fun onOpenFullScreen(p0: NendAdNativeMediaView?) {
            }
            override fun onCloseFullScreen(p0: NendAdNativeMediaView?) {
            }
            override fun onStartPlay(p0: NendAdNativeMediaView?) {
            }
            override fun onError(p0: Int, p1: String?) {
            }

            override fun onCompletePlay(mediaView: NendAdNativeMediaView?) {
                runnable = Runnable{
                    if(checkFragmentListener != null && checkFragmentListener!!.onCheckedCurrentFragment() == tag_diagnose)
                    {
                        nendLoader = NendAdNativeVideoLoader(context, NEND_VIDEO_ID,NEND_VIDEO_KEY)
                        loadNendVideoAd()
                    }
                }

                handler.postDelayed(runnable, 1000)
            }
        })
    }
}