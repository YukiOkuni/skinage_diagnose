package com.example.skinagediagnose

class MyListener {

    interface ChangeFragmentListener
    {
        fun onChangeFragment(tag: String)
    }

    interface ChangeQuizListener
    {
        fun onChangeQuiz(currentQuizNum: Int)
    }

    interface FailedLoadQuizListener
    {
        fun onFailedLoadQuiz()
    }

    interface CheckedCurrentFragmentListener
    {
        fun onCheckedCurrentFragment(): String
    }
}