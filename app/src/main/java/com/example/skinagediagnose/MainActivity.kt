package com.example.skinagediagnose

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.flurry.android.FlurryAgent
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*

const val HANAUTA_APP: String = "https://play.google.com/store/apps/dev?id=7172322860791797110"
const val STORE_URL: String = "https://play.google.com/store/apps/details?id=jp.co.hnut.skin_age"
const val MAIL_ADDRESS: String = "hanauta.app@gmail.com"
const val APP_VERSION: Double = 2.0

const val FLURRY_API = "67Q3RZY7QS69JKB37F27"

const val tag_top = "top"
const val tag_diagnose = "diagnose"
const val tag_playAd = "playAd"
const val tag_result = "result"

const val DONE_REVIEW = "DONE_REVIEW_SKINAGE"
const val LAST_DIALOG = "LAST_DIALOG_SKINAGE"
const val oneDay: Long = 24 * 60 * 60 * 1000L

class MainActivity : AppCompatActivity(),
    MyListener.ChangeFragmentListener,
    MyListener.ChangeQuizListener,
    MyListener.FailedLoadQuizListener,
    MyListener.CheckedCurrentFragmentListener {

    var menuItem: MenuItem? = null
    var dialog: AlertDialog? = null

    var token = ""

    var currentFragment = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(main_toolbar)

        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_fragment, TopFragment() as Fragment, tag_top)
            .add(R.id.main_fragment, DiagnoseFragment() as Fragment, tag_diagnose)
            .add(R.id.main_fragment, PlayAdFragment() as Fragment, tag_playAd)
            .add(R.id.main_fragment, ResultFragment() as Fragment, tag_result)
            .commitAllowingStateLoss()
    }

    override fun onChangeFragment(tag: String) {
        when (tag) {
            tag_top -> {
                currentFragment = tag_top
                main_toolbar_text.text = "肌年齢診断"
                quiz_count.text = ""
                menuItem?.isVisible = true
                (supportFragmentManager.findFragmentByTag(tag) as TopFragment).onFragmentStart()
            }

            tag_diagnose -> {
                (supportFragmentManager.findFragmentByTag(tag_playAd) as PlayAdFragment).loadVideoAd()
                currentFragment = tag_diagnose
                main_toolbar_text.text = "診断中..."
                menuItem?.isVisible = false
                (supportFragmentManager.findFragmentByTag(tag) as DiagnoseFragment).onFragmentStart()
            }

            tag_playAd -> {
                currentFragment = tag_playAd
                main_toolbar_text.text = "集計を開始しています..."
                quiz_count.text = ""
                menuItem?.isVisible = false
                (supportFragmentManager.findFragmentByTag(tag) as PlayAdFragment).onFragmentStart()
            }

            tag_result -> {
                currentFragment = tag_result
                main_toolbar_text.text = "あなたの肌年齢は..."
                quiz_count.text = ""
                menuItem?.isVisible = false
                (supportFragmentManager.findFragmentByTag(tag) as ResultFragment).onFragmentStart()
            }

            else -> {
                currentFragment = ""
                main_toolbar_text.text = "肌年齢診断"
                quiz_count.text = ""
                menuItem?.isVisible = true
            }
        }
    }

    override fun onChangeQuiz(currentQuizNum: Int) {
        quiz_count.text = "${currentQuizNum + 1} / ${SkinAgeContract.quizSet.size}"
    }

    override fun onFailedLoadQuiz() {
        popErrorDialog()
    }

    override fun onCheckedCurrentFragment():String {
        return currentFragment
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_top, menu)

        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menuItem = menu?.findItem(R.id.detail_button) as MenuItem

        menuItem?.isVisible = true

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.detail_button -> {
                val intent = Intent(this, MenuActivity::class.java)
                startActivity(intent)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()

        FlurryAgent.Builder()
            .withLogEnabled(true)
            .build(this, FLURRY_API)

        setToken()

        popReviewDialog()
    }

    override fun onStop() {
        super.onStop()

        FlurryAgent.onEndSession(this)
    }

    override fun onPause() {
        super.onPause()

        if (dialog != null)
            dialog?.dismiss()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            popConfirmationBackDialog()
            return false
        }

        return super.onKeyDown(keyCode, event)
    }

    fun setToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (task.isSuccessful) {
                    if (task.result?.token != null) {
                        token = task.result?.token!!

                        SkinAgeContract.setUserToken(token)
                    }
                } else {
                    return@OnCompleteListener
                }
            })
    }

    fun popReviewDialog() {
        val preference_done = getSharedPreferences(DONE_REVIEW, Context.MODE_PRIVATE)
        val doneReview = preference_done.getBoolean(DONE_REVIEW, false)

        val preference_day = getSharedPreferences(LAST_DIALOG, Context.MODE_PRIVATE)
        val lastDay = preference_day.getLong(LAST_DIALOG, 0L)

        if (!doneReview && lastDay + oneDay < System.currentTimeMillis()) {

            val editor_day = preference_day.edit()
            editor_day.putLong(LAST_DIALOG, System.currentTimeMillis())
            editor_day.apply()

            val alertDialog = AlertDialog.Builder(this)

            alertDialog.setTitle("レビューのお願い")
                .setMessage(
                    "ＤＬしていただき ありがとうございます！\n" +
                            "\n" +
                            "よろしければ応援コメントを\n" +
                            "おねがいします！！！"
                )
                .setPositiveButton("はい", DialogInterface.OnClickListener { dialogInterface, i ->
                    val editor_done = preference_done.edit()
                    editor_done.putBoolean(DONE_REVIEW, true)
                    editor_done.apply()

                    try {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(STORE_URL))
                        startActivity(intent)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(this, "移動しようとしているページを開くためのアプリがないようです。", Toast.LENGTH_SHORT).show()
                    }
                })
                .setNegativeButton("いいえ", null)

            dialog = alertDialog.show()
        }

    }

    fun popErrorDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setTitle("通信エラー")
            .setMessage("下記のいずれかをお試しください。\n\n①電波の良い場所でリトライ\n②時間をおいてリトライ\n③再起動してリトライ\n")
            .setNegativeButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                val intent = Intent(this, SplashActivity::class.java)
                startActivity(intent)
            })
        dialog = alertDialog.show()
    }

    fun popConfirmationBackDialog() {
        val alertDialog = AlertDialog.Builder(this)

        alertDialog.setTitle("診断を終了しますか？")
            .setMessage("肌年齢診断を終了してホーム画面に戻ります。よろしいですか？")
            .setPositiveButton("いいえ", DialogInterface.OnClickListener { dialogInterface, i -> })
            .setNegativeButton("はい", DialogInterface.OnClickListener { dialogInterface, i ->
                val homeIntent = Intent(Intent.ACTION_MAIN)
                homeIntent.addCategory(Intent.CATEGORY_HOME)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(homeIntent)
            })

        dialog = alertDialog.show()
    }
}