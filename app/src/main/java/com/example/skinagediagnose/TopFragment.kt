package com.example.skinagediagnose

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_top.*

class TopFragment : Fragment() {
    private val handler = Handler()
    private lateinit var runnable: Runnable

    private var changeListener: MyListener.ChangeFragmentListener? = null
    private var failedListener: MyListener.FailedLoadQuizListener? = null

    var loadingNum = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_top, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        changeListener = context as MyListener.ChangeFragmentListener
        failedListener = context as MyListener.FailedLoadQuizListener
    }

    override fun onDetach() {
        super.onDetach()

        if (::runnable.isInitialized)
            handler.removeCallbacks(runnable)

        changeListener = null
    }

    override fun onStart() {
        super.onStart()

        go_diagnose.setOnClickListener(View.OnClickListener { view: View ->
            loadingNum = 0

            goDiagnose()
        })
    }

    fun goDiagnose() {
        if (loadingNum >= 15) {
            failedListener?.onFailedLoadQuiz()
            if (::runnable.isInitialized)
                handler.removeCallbacks(runnable)
        } else {
            if (SkinAgeContract.quizAll.size == SkinAgeContract.quizAllNum &&
                SkinAgeContract.resultSet.size == SkinAgeContract.resultNum
            ) {
                changeListener?.onChangeFragment(tag_diagnose)

                if (::runnable.isInitialized)
                    handler.removeCallbacks(runnable)

                fragmentManager
                    ?.beginTransaction()
                    ?.hide(fragmentManager?.findFragmentByTag(tag_top)!!)
                    ?.show(fragmentManager?.findFragmentByTag(tag_diagnose)!!)
                    ?.commitAllowingStateLoss()
            } else {
                loadingNum++

                go_diagnose.text = "ロード中..."

                runnable = Runnable {
                    goDiagnose()
                }

                handler.postDelayed(runnable, 100)
            }
        }
    }

    fun onFragmentStart() {
        go_diagnose.text = "スタート"
    }
}