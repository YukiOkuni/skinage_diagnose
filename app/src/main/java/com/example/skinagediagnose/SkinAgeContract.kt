package com.example.skinagediagnose

import android.annotation.SuppressLint
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot

class SkinAgeContract {
    companion object {
        var quizAll: MutableList<Array<String>> = mutableListOf()
        var quizSet: MutableList<Array<String>> = mutableListOf()
        var resultSet: MutableList<Array<String>> = mutableListOf()

        @SuppressLint("StaticFieldLeak")
        val db = FirebaseFirestore.getInstance()

        val quizAllNum = 38
        val quizNum = 30
        val resultNum = 5

        val COLLECTION = "series"
        val DOCUMENT = "skinage"
        val SUB_COL_QUIZ = "quizSet"
        val SUB_COL_RESULT = "resultSet"
        val SUB_COL_UID = "userId"
        val FIELD_QUIZ = "q"
        val FIELD_ANS1 = "a1"
        val FIELD_ANS2 = "a2"
        val FIELD_ANS3 = "a3"
        val FIELD_RANGE = "range"
        val FIELD_RESULT = "result"
        val FIELD_UID = "userToken"

        fun getQuizData() {
            db.collection(COLLECTION)
                .document(DOCUMENT)
                .collection(SUB_COL_QUIZ)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (document: QueryDocumentSnapshot in task.result!!) {
                            quizAll.add(
                                arrayOf(
                                    document.data.get(FIELD_QUIZ).toString().replace("。", "。\n").replace("[", "").replace("]",""),
                                    document.data.get(FIELD_ANS1).toString().replace("[", "").replace("]",""),
                                    document.data.get(FIELD_ANS2).toString().replace("[", "").replace("]",""),
                                    document.data.get(FIELD_ANS3).toString().replace("[", "").replace("]","")
                                )
                            )
                        }
                    } else {
                        throw Exception(task.exception)
                    }
                }
                .addOnFailureListener { e -> throw e }
        }

        fun getResultData()
        {
            db.collection(COLLECTION)
                .document(DOCUMENT)
                .collection(SUB_COL_RESULT)
                .get()
                .addOnCompleteListener { task ->
                    if(task.isSuccessful)
                    {
                        for (document: QueryDocumentSnapshot in task.result!!)
                        {
                            resultSet.add(
                                arrayOf(document.data.get(FIELD_RANGE).toString().replace("[", "").replace("]",""),
                                    document.data.get(FIELD_RESULT).toString().replace("[", "").replace("]","").replace("\"", ""))
                            )
                        }
                    }
                    else
                    {
                        throw Exception(task.exception)
                    }
                }
                .addOnFailureListener { e -> throw e }
        }

        fun setUserToken(token: String)
        {
            val userToken: Map<String, String> = mapOf(FIELD_UID to token)

            db.collection(COLLECTION)
                .document(DOCUMENT)
                .collection(SUB_COL_UID)
                .document(token)
                .set(userToken)
                .addOnSuccessListener { documentReference -> }
                .addOnFailureListener { e -> throw e }
        }
    }
}