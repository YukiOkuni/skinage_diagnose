package com.example.skinagediagnose

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseMessageReceiver: FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val builder = NotificationCompat.Builder(this, resources.getString(R.string.default_notification_channel_id))

        builder.setContentTitle(remoteMessage?.notification?.title ?: "タイトルなし")
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setSmallIcon(R.drawable.ic_launcher_foreground)//ここは変更する
            .setContentText(remoteMessage?.notification?.body ?: "テキストなし")
            .setDefaults(Notification.DEFAULT_ALL)
            .setAutoCancel(true)
            .setWhen(System.currentTimeMillis())

        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        builder.setContentIntent(pendingIntent)

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val channel = NotificationChannel(resources.getString(R.string.default_notification_channel_id), "SkinAge", NotificationManager.IMPORTANCE_DEFAULT)
            manager.createNotificationChannel(channel)
        }

        manager.notify(0, builder.build())
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }
}