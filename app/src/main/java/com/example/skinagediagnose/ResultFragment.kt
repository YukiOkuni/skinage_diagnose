package com.example.skinagediagnose

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_result.*

class ResultFragment : Fragment() {

    private var changeListener: MyListener.ChangeFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        fragmentManager
            ?.beginTransaction()
            ?.hide(fragmentManager?.findFragmentByTag(tag_result)!!)
            ?.commitAllowingStateLoss()

        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        changeListener = context as MyListener.ChangeFragmentListener
    }

    override fun onDetach() {
        super.onDetach()

        changeListener = null
    }

    override fun onStart() {
        super.onStart()


    }

    fun checkResult(score: Int) {
        val range: MutableList<List<String>> = mutableListOf()

        var i = 0
        while (i < SkinAgeContract.resultSet.size) {
            range.add(SkinAgeContract.resultSet[i][0].split("～"))

            if (score >= range[i][0].toInt() && score <= range[i][1].toInt())
                break

            i++
        }

        val from = SkinAgeContract.resultSet[i][1].indexOf("【")
        val to = SkinAgeContract.resultSet[i][1].indexOf("】")

        val degree = SkinAgeContract.resultSet[i][1].substring(from + 1, to)

        val describe = SkinAgeContract.resultSet[i][1].substring(to + 3)

        skinage_degree.text = degree
        result_describe.text = describe
    }

    fun onFragmentStart() {
        checkResult(arguments?.getInt(SCORE)!!)

        retry_button.setOnClickListener ( View.OnClickListener { view: View ->
            changeListener?.onChangeFragment(tag_diagnose)

            fragmentManager
                ?.beginTransaction()
                ?.hide(fragmentManager?.findFragmentByTag(tag_result)!!)
                ?.show(fragmentManager?.findFragmentByTag(tag_diagnose)!!)
                ?.commitAllowingStateLoss()
        } )

        share_button.setOnClickListener( View.OnClickListener { view: View ->
            val sharemsg: String = Uri.encode("あなたの #肌年齢 は「${skinage_degree.text}」です\n\n簡単な質問に答えるだけで自分の肌年齢が分かる #診断アプリ は #肌年齢診断 \n\n→${STORE_URL}")

            try{
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("twitter://post?message=${sharemsg}")
                startActivity(intent)
            }catch (e:ActivityNotFoundException)
            {
                val share = Intent(Intent.ACTION_SEND)
                share.setType("text_plain")
                share.putExtra(Intent.EXTRA_TEXT, sharemsg)
                startActivity(Intent.createChooser(share, "どのアプリでシェアしますか？"))
            }
        })

        top_button.setOnClickListener(View.OnClickListener { view: View ->
            changeListener?.onChangeFragment(tag_top)

            fragmentManager
                ?.beginTransaction()
                ?.hide(fragmentManager?.findFragmentByTag(tag_result)!!)
                ?.show(fragmentManager?.findFragmentByTag(tag_top)!!)
                ?.commitAllowingStateLoss()
        })
    }
}