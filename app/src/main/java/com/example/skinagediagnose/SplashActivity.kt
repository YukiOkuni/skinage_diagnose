package com.example.skinagediagnose

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {

    private val handler = Handler()
    private lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        SkinAgeContract.quizSet = mutableListOf()
        SkinAgeContract.resultSet = mutableListOf()

        SkinAgeContract.getQuizData()
        SkinAgeContract.getResultData()
    }

    override fun onResume() {
        super.onResume()

        runnable = Runnable {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        handler.postDelayed(runnable, 2000)
    }

    override fun onPause() {
        super.onPause()

        handler.removeCallbacks(runnable)
    }
}
