package com.example.skinagediagnose

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        setSupportActionBar(menu_toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        return super.onPrepareOptionsMenu(menu)
    }

    fun goHanauta(view: View) {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(HANAUTA_APP))
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "移動しようとしているページを開くためのアプリがないようです。", Toast.LENGTH_SHORT).show()
        }
    }

    fun goReview(view: View) {
        try {
            val preference_done = getSharedPreferences(DONE_REVIEW, Context.MODE_PRIVATE)
            val editor_done = preference_done.edit()
            editor_done.putBoolean(DONE_REVIEW, true)
            editor_done.apply()

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(STORE_URL))
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "移動しようとしているページを開くためのアプリがないようです。", Toast.LENGTH_SHORT).show()
        }
    }

    fun goContact(view: View) {
        try {
            val mailBody: String = "\n\n\n\n\n\n\n\n\n\n" +
                    "＊＊ここから下は消さないでください＊＊\n" +
                    "App ver：" + BuildConfig.VERSION_NAME + "\n" +
                    "OS：Android " + Build.VERSION.RELEASE + "\n" +
                    "Device：" + Build.DEVICE + "\n"
            val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$MAIL_ADDRESS"))
            intent.putExtra(Intent.EXTRA_SUBJECT, "肌年齢診断(A)について")
            intent.putExtra(Intent.EXTRA_TEXT, mailBody)
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(this, "移動しようとしているページを開くためのアプリがないようです。", Toast.LENGTH_SHORT).show()
        }
    }
}
